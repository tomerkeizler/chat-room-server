require("dotenv").config();
const {
  API_PORT = 3030,
  API_HOST,
  CLIENT_URL,
  CHAT_ROOM_SYSTEM_MESSAGE,
  CHAT_MESSAGE,
  USER_TYPING,
  USER_STOPPED_TYPING,
} = process.env;

const log = require("@ajar/marker")
const server = require("http").createServer();
const io = require("socket.io")(server, {
  cors: {
    origin: CLIENT_URL,
    methods: ["GET", "POST"]
  }
});

// ----------------------------
// ----------------------------

io.on("connection", (socket) => {
  // getting room ID
  const { ROOM_ID } = socket.handshake.query;
  
  // join a chat room
  socket.join(ROOM_ID);
  socket.to(ROOM_ID).emit(CHAT_ROOM_SYSTEM_MESSAGE, {
    body: `${socket.id} has joined the room`,
  });
  log.blue(`${socket.id} has joined to the ${ROOM_ID} room`)

  // listen to chat messages
  socket.on(CHAT_MESSAGE, (data) => {
    io.in(ROOM_ID).emit(CHAT_MESSAGE, data);
    log.green('new message in room', ROOM_ID)
  });

  // listen to typing start
  socket.on(USER_TYPING, (data) => {
    socket.to(ROOM_ID).emit(USER_TYPING, data);
    log.cyan('user started typing in room', ROOM_ID)
  });

  // listen to typing stop
  socket.on(USER_STOPPED_TYPING, (data) => {
    io.in(ROOM_ID).emit(USER_STOPPED_TYPING, data);
    log.cyan('user stopped typing in room', ROOM_ID)
  });

  // leave a chat room
  socket.on("disconnect", () => {
    socket.leave(ROOM_ID);
    socket.to(ROOM_ID).emit(CHAT_ROOM_SYSTEM_MESSAGE, {
      body: `${socket.id} has left the room`,
    });
    log.red(`${socket.id} has left the ${ROOM_ID} room`)
  });
});

// ----------------------------
// ----------------------------

// start the server
(async () => {
  await server.listen(API_PORT, API_HOST);
  log.magenta(
    `server is live on`,
    ` ✨ ⚡ http://${API_HOST}:${API_PORT}  ✨ ⚡`
  );
})().catch((error) => log.error(error));
